Gibbs Sampling: Probit regression
=================================

TODO
----
 * Fix code
 * Add graphical model for this example

Links
-------
 * Raw videos starting from Gibbs sampler description and Probit regression starts at 12:38 in video:
    [Sara's Gibbs probit example](http://www.youtube.com/watch?v=rsUt9uV6j70&feature=share&list=PLrM7Z8xNORRdvGS6qEkbNmXavtutAEEeG)
 * [Sara's talk about Probit Gibbs sampling](http://youtu.be/rsUt9uV6j70?t=12m18s)
 * [Sara's slides on Sampling methods](https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxmaWxpcGp1cmNpY2VrfGd4OjZmNmVjYjg3ODJkMjcwODY)
 * [Website of the course Bayesian Inference](https://sites.google.com/site/filipjurcicek/teaching/bayesian-inference)
