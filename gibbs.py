#
# some header
#

import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
from scipy.stats import truncnorm
from itertools import izip


class GibbsSamler(object):
    """GibbsSampler class implements
    methods for sampling from 2 marginal distribution
    of bivariate probit model.

    It stores all the initial values needed
    for computing the marginals"""

    def __init__(self, X, Y, saveW=False, saveYstar=False):
        """
        :X: X are regressors (features). Each row represents one bivariate sample.
        :Y: Y is the dependent random variable.
        :saveYstar: If True saving values from the marginal Ystar.
        :saveW: If True saving values from the marginal W.
        """
        self._saveW = saveW
        self._saveYstar = saveYstar
        self.Ystars = []
        self.Ws = []

        self.X = X.T
        self.Y = Y.T
        # Number of observations and t-number of variables
        self.n, t = self.X.shape
        assert self.n == len(
            Y) and t == 2, 'BIVARIATE len(features) must be equal len(observed_values)'

        # Initial parameters for sampling first weights. See init_vars(self)
        # Setting covarience matrix and mean to any particular value is not limiting.
        # Vector of weights can simulate any values
        self.b0, self.B0 = np.zeros(t), np.eye(t)
        # matrix operations: inv - inverse, T transpose, dot - product
        self.B = inv(inv(self.B0) + self.X.T.dot(self.X))
        self._invB0b0 = np.squeeze(np.asarray(
            inv(self.B0).dot(self.b0)))

    def init_vars(self):
        return np.random.multivariate_normal(self.b0, self.B0)

    def sampleYstar(self, w):
        """
        :w: weights in bivariate linear regression
        :returns: vector sample from marginal Y*|X,w
        """
        means_i = self.X.dot(w)
        shiftY = 2 * self.Y - 1  # Replace 0,1 with -1, 1 in Y to shiftY
        clib_b = shiftY * 100 * np.ones(self.n) + means_i

        # a, b = (clip_a - mean) / std, (clip_b - mean) / std
        # std=1 and 2 cases: clip_a,clib_b= mean-100,mean | clip_a,clib_b= mean,mean+100
        a = np.minimum(means_i, clib_b)
        b = np.maximum(means_i, clib_b)
        # print 'Y a', a
        # print 'Y b', b
        assert (a < b).all(), 'every clip a should be smaller than clib b'
        ystar = truncnorm.rvs(a, b, self.n)

        if self._saveYstar:
            self.Ystars.append(ystar)
        return ystar

    def sampleW(self, ystar):
        """
        :ystar: vector of {0,1}; len(ystar) == self.n
        :returns: tuple sample w=(w1, w0) from marginal w |Y*
        """
        # print 'W ystar', ystar
        # print 'W self.X.T', self.X.T
        # print 'W self._invB0b0, self.X.T.dot(ystar)', self._invB0b0, self.X.T.dot(ystar)
        newmean = np.nan_to_num(np.squeeze(np.asarray(
            self.B.dot(self._invB0b0 + self.X.T.dot(ystar))
        )))
        covariance = self.B
        # print 'sampleW newmean', newmean, 'cov', covariance
        W = np.random.multivariate_normal(newmean, covariance)

        if self._saveW:
            self.Ws.append(W)
        return W


def gibbs(X, Y, num_iter):
    '''X is matrix(2, num_observation)
         X are regressors (features). Each row represents one bivariate sample.
       Y is vector(1, num_observation) with values 0 or 1.
         Y is the dependent random variable.
    We are interested in sampling w0, w1 weights for the linear regression.'''
    gs = GibbsSamler(X, Y, saveW=True, saveYstar=True)
    w = gs.init_vars()
    for i in xrange(num_iter):
        ystar = gs.sampleYstar(w)
        w = gs.sampleW(ystar)

    return gs


def generate_observation(num_samples,
                         weights=np.array([2, 1]),
                         noise_mean=np.array([0, 0]),
                         noise_cov=np.matrix([[2, 0.2], [0.2, 2]])):
    x = np.linspace(-5, 5, num_samples)
    w1, w0 = weights
    y = w1 * x + w0
    X = np.vstack((x, y))
    noise = np.random.multivariate_normal(noise_mean, noise_cov, num_samples)
    X = X + noise.T
    # point x[i], y[i] is above decision line if and only if:
    Y = weights[0] * noise[:, 0] + weights[1] > noise[:, 1]
    # converting True and False to 0 and 1
    Y = Y.astype(int)
    return (X, Y)


def plot_regression(data2D, w, Y=None):
    x0, x1 = data2D
    w1, w0 = w
    minx, maxx = np.min(x0), np.max(x0)
    x_points = np.linspace(minx, maxx, len(x0))
    decision_line = w1 * x_points + w0
    if Y is None:
        Y = w1 * x0 + w0 > x1
    Y = Y.astype(bool)
    notY = np.invert(Y)
    greenX, greenY = x0[Y], x1[Y]
    blueX, blueY = x0[notY], x1[notY]
    assert len(greenX) + len(blueX) == len(x0)
    assert len(greenY) + len(blueY) == len(x1)
    plt.axis('equal')
    plt.plot(x_points, decision_line, 'r--', greenX, greenY, 'gx', blueX, blueY, 'bx')
    plt.show()


def plot_gibbs(gibbs_sampler):
    yStars = gibbs_sampler.Ystars
    ws = gibbs_sampler.Ws
    X = gibbs_sampler.X.T
    for weights, ystar_sample in izip(ws, yStars):
        plot_regression(X, weights, ystar_sample)


if __name__ == '__main__':
    n_iter = 2000
    n_samples = 500
    weights = np.array([2, 1])

    X, Y = generate_observation(n_samples, weights)
    plot_regression(X, weights, Y)
    gs = gibbs(X, Y, n_iter)
    # plot_gibbs(gs)

    lastW, lastYstar = gs.Ws[-1], gs.Ystars[-1]
    plot_regression(X, lastW, lastYstar)


#############################################################################
#     TODO show the older simpler version of sampleYstar and truncNorm
#     def sampleYstar(self, w):
#             """
#             :w: weights in bivariate linear regression
#             :returns: vector sample from marginal Y*|X,w
#             """
#             ystar = np.empty((self.n, 1))
#             # not very effective
#             for i in xrange(self.n):
#                 mean_i = (self.X[:, i] * w)
#                 positiveOnly = self.Y[i] > 0
#                 ystar[i] = self.truncNorm(mean_i, 1, positiveOnly)
#
# def truncNorm(mean, cov, positiveOnly=True):
#     '''simple rejection sampling -> very problematic'''
#     while True:
#         x = np.random.normal(mean, cov)
#         positive = x > 0.0
#         print 'trunc', x, mean, cov, positiveOnly, positive
#         if (positiveOnly and positive) or (not positiveOnly and not positive):
#             break
#     return x
